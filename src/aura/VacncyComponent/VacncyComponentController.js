({
    doInit: function (component, event, helper) {
        let action = component.get("c.getCandidates");
        action.setParams({"vacancyId": component.get("v.recordId")});

        action.setCallback(this, function (response) {
            component.set("v.suggestedCandidates", response.getReturnValue());
        });
        $A.enqueueAction(action);
    }
});