public with sharing class InterviewCreateExtension {

    private final Interview__c interview;

    public List<Id> contactIds { get; set; }

    public Vacancy__c selectedVacancy {
        get {
            if (null == this.selectedVacancy) {
                this.selectedVacancy = [SELECT Id, Primary_Skill__c, Account__c FROM Vacancy__c WHERE Id = :THIS.interview.Vacancy__c];
            }
            return this.selectedVacancy;
        }
        set;
    }

    public InterviewCreateExtension(ApexPages.StandardController stdController) {
        this.interview = (Interview__c) stdController.getRecord();
    }

    public SelectOption[] getVacancies() {
        SelectOption[] vacanciesOptions = new SelectOption[]{
        };
        for (Vacancy__c vacancy : [SELECT Id, Title__c FROM Vacancy__c WHERE Closed__c = FALSE]) {
            vacanciesOptions.add(new SelectOption(vacancy.Id, vacancy.Title__c));
        }
        return vacanciesOptions;
    }

    public SelectOption[] getCandidates() {
        SelectOption[] candidateSelectOptions = new SelectOption[]{
        };
        for (Candidate__c candidate : [SELECT Id, Full_Name__c FROM Candidate__c WHERE Primary_Skill__c = :THIS.selectedVacancy.Primary_Skill__c]) {
            candidateSelectOptions.add(new SelectOption(candidate.Id, candidate.Full_Name__c));
        }
        return candidateSelectOptions;
    }

    public SelectOption[] getContacts() {
        SelectOption[] contactsOptions = new SelectOption[]{
        };
        for (Contact contact : [SELECT Id, FirstName, LastName FROM Contact WHERE Account.Id = :THIS.selectedVacancy.Account__c]) {
            contactsOptions.add(new SelectOption(contact.Id, contact.FirstName + contact.LastName));
        }
        return contactsOptions;
    }

    public PageReference reloadRegion() {
        return null;
    }


    @RemoteAction
    public static Vacancy__c getVacancy(Id vacancyId) {
        Vacancy__c vacancy = [
                SELECT Id, Name, Title__c, Primary_Skill__c, Short_Description__c, Minimum_Salary__c, Maximum_Salary__c, Account__r.Name
                FROM Vacancy__c
                WHERE Id = :vacancyId
        ];
        return vacancy;
    }

    public PageReference save() {
        this.interview.Status__c = 'New';
        insert this.interview;
        Feedback__c[] feedbacks = new Feedback__c[]{
        };
        for (Contact contact : [SELECT Id FROM Contact WHERE Id IN :THIS.contactIds]) {
            feedbacks.add(new Feedback__c(Interview__c = this.interview.Id, Interviewer__c = contact.Id));
        }
        insert feedbacks;
        PageReference pr = new PageReference('/' + this.interview.Id);
        return pr;
    }
}