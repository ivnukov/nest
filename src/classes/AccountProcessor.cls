global class AccountProcessor {
//Created for trailhead.
    @Future
    public static void countContacts(List<Id> ids) {
        List<Account> accList = [
                SELECT Id, Name, Number_of_Contacts__c, (SELECT Id FROM Contacts)
                FROM Account
                WHERE Id IN :ids
        ];
        for (Account acc : accList) {
            acc.Number_of_Contacts__c = acc.Contacts.size();
        }
        update accList;
    }
}