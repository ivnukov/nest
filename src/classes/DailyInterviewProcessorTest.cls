@IsTest
public with sharing class DailyInterviewProcessorTest {

    @IsTest
    static void testIncompleteInterviewProcessorSchedule() {
        Date d = Date.today().addDays(1);
        Time t = Time.newInstance(8, 0, 0, 0);
        Datetime dt = Datetime.newInstance(d.year(), d.month(), d.day(), t.hour(), t.minute(), t.second());
        System.debug('System dt: ' + Datetime.now());
        String CRON = '0 0 8 * * ? *';
        Test.startTest();
        Id jobId = System.schedule('DailyFeedbackMonitoring', CRON, new DailyInterviewProcessor());
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE Id = :jobId];

        Test.stopTest();
        System.assertEquals(0, ct.TimesTriggered);
        System.assertEquals(String.valueOf(dt), String.valueOf(ct.NextFireTime));
    }

}