global class LeadProcessor implements Database.Batchable<SObject>, Database.Stateful {
//Created for trailhead.
    global Integer leadsProcessed = 0;

    global Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator([SELECT Id, LeadSource FROM Lead]);
    }

    global void execute(Database.BatchableContext bc, List<Lead> leadList) {
        for (Lead lead : leadList) {
            lead.LeadSource = 'Dreamforce';
            leadsProcessed++;
        }
        update leadList;
    }

    global void finish(Database.BatchableContext bc) {
        System.debug(leadsProcessed + ' leads processed');
    }
}