@IsTest
public class AddPrimaryContactTest {
//Created for trailhead.
    @TestSetup
    static void setup() {
        List<Account> accountsList = new List<Account>();
        for (Integer i = 0; i < 50; i++) {
            accountsList.add(new Account(Name = 'Account ' + 'NY' + i, BillingState = 'NY'));
            accountsList.add(new Account(Name = 'Account ' + 'CA' + i, BillingState = 'CA'));
        }
        insert accountsList;
    }

    @IsTest
    static void testAddPrimaryContact() {
        Contact newContact = new Contact(
                LastName = 'Test Contact');
        insert newContact;
        AddPrimaryContact updateJob = new AddPrimaryContact(newContact, 'CA');
        Test.startTest();
        System.enqueueJob(updateJob);
        Test.stopTest();
        System.assertEquals(50, [SELECT COUNT() FROM Contact WHERE Account.BillingState = 'CA']);
    }
}