public with sharing class InterviewHandler {

    public void updateVacancy() {

        List<Vacancy__c> updatedVacancies = new List<Vacancy__c>();

//       Get the map of updated interviews with vacancies data
        Map<Id, Interview__c> interviewVacancyMap = new Map<Id, Interview__c>([
                SELECT Id, Vacancy__r.Id, Vacancy__r.Closed__c, Vacancy__r.Submitted_Candidate__c
                FROM Interview__c
                WHERE Id IN :Trigger.newMap.keySet()
        ]);
        for (Interview__c interview : (List<Interview__c>) Trigger.new) {
//          Iterate over interviews update required vacancies and add it to List for update;
            Interview__c oldInterview = ((Map<Id, Interview__c>) Trigger.oldMap).get(interview.Id);

            if (interview.Result__c == 'Hired' && oldInterview.Result__c != 'Hired') {
                Vacancy__c vacancy = interviewVacancyMap.get(interview.Id).Vacancy__r;
//               validate is not null
                vacancy.Closed__c = true;
                vacancy.Submitted_Candidate__c = interview.Candidate__c;
                updatedVacancies.add(vacancy);
            } else if (oldInterview.Result__c == 'Hired' && interview.Result__c != 'Hired') {
//              TODO: move to validation rule
                interview.addError('You can\'t change interview state.');
            }
        }
        update updatedVacancies;
    }
    public void validateFeedback() {

        Map<Id, Interview__c> interviewsMap = new Map<Id, Interview__c>([
                SELECT Id, (SELECT Id FROM Feedbacks__r)
                FROM Interview__c
                WHERE Id IN :Trigger.newMap.keySet()
        ]);
        for (Interview__c interview : (List<Interview__c>) Trigger.new) {

            if (interview.Result__c != null) {
                Interview__c interviewObj = interviewsMap.get(interview.Id);
                if (interviewObj.Feedbacks__r.size() < 3) {
                    interview.addError('Interview should have at least three feedback entries to be closed');
                }
            }
        }
    }
}