global class DailyInterviewProcessor implements Schedulable {
    global void execute(SchedulableContext ctx) {
        Database.executeBatch(new IncompleteInterviewProcessor());
    }
}