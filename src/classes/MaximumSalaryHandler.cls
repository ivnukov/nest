public with sharing class MaximumSalaryHandler {
    public void setMaxSalary() {
        for (Vacancy__c vacancy : (List<Vacancy__c>) Trigger.new) {
            if (vacancy.Maximum_Salary__c == null) {
                vacancy.Maximum_Salary__c = vacancy.Minimum_Salary__c;
            }
        }
    }
}