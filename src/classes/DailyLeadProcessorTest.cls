@IsTest
public with sharing class DailyLeadProcessorTest {
//Created for trailhead.
    @TestSetup
    static void setup() {
        List<Lead> leads = new List<Lead>();
        for (Integer i = 0; i < 200; i++) {
            leads.add(new Lead(LastName = 'Lead ' + i,
                    Company = 'Company ' + i));
        }
        insert leads;
    }

    @IsTest
    static void testDailyLeadProcessor() {
        String CRON = '0 0 8 * * ? *';
        Test.startTest();
        DailyLeadProcessor leadUpdater = new DailyLeadProcessor();
        System.schedule('DailyLeadProcess', CRON, leadUpdater);
        Test.stopTest();
        System.assertEquals(200, [SELECT COUNT() FROM Lead WHERE LeadSource = 'Dreamforce']);
    }
}