global class IncompleteInterviewProcessor implements Database.Batchable<Interview__c>, Database.Stateful {
    global Integer interviewsOutdated = 0;

    private Map<Id, Integer> excludedInterviews = new Map<Id, Integer>();

    private void getExcludedInterviews() {

        List<AggregateResult> aggRes = [
                SELECT Interview__c, COUNT(Id)
                FROM Feedback__c
                WHERE Completed__c = TRUE AND Interview__r.Result__c = NULL
                GROUP BY Interview__c
        ];

        for (AggregateResult res : aggRes) {
            Integer feedbacksCount = Integer.valueOf(res.get('expr0'));
            if (feedbacksCount >= 3) {
                excludedInterviews.put((Id) res.get('Interview__c'), feedbacksCount);
            }
        }
    }

    global List<Interview__c> start(Database.BatchableContext bc) {

        Datetime dt = Datetime.now().addDays(-2);
        this.getExcludedInterviews();
        List<Interview__c> interviewList = [
                SELECT Id, (SELECT Id, Interviewer__r.Name FROM Feedbacks__r WHERE Completed__c = FALSE), Vacancy__r.Assignee__c
                FROM Interview__c
                WHERE Status__c = 'completed' AND Interview_Time__c <= :dt AND Result__c = NULL
        ];
        return interviewList;
    }

    global void execute(Database.BatchableContext bc, List<Interview__c> interviews) {
        System.debug('interv: ' + interviews.size());
        List<Task> tasks = new List<Task>();

        for (Interview__c interview : interviews) {
            if (interview.Feedbacks__r.size() > 0 && this.excludedInterviews.get(interview.Id) == null) {
                String interviewers = '';
                for (Feedback__c feedback : interview.Feedbacks__r) {
                    interviewers = interviewers + feedback.Interviewer__r.Name + '\n';
                }
                tasks.add(new Task(
                        OwnerId = interview.Vacancy__r.Assignee__c,
                        Subject = 'Feedback required for Interview ' + interview.Id,
                        Description = 'Incomplete feedback by: \n' + interviewers
//                       TODO WhatId = interview.Id
                ));

            }
        }
        interviewsOutdated += tasks.size();
        insert tasks;
    }

    global void finish(Database.BatchableContext bc) {

        System.debug('Tasks for ' + interviewsOutdated + ' interviews created.');
    }
}