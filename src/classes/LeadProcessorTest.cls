@IsTest
public with sharing class LeadProcessorTest {
//Created for trailhead.
    @TestSetup
    static void setup() {
        List<Lead> newLeads = new List<Lead>();

        for (Integer i = 0; i < 200; i++) {
            newLeads.add(new Lead(LastName = 'Lead ' + i,
                    Company = 'Company ' + i,
                    Status = 'Open - Not Contacted'));
        }
        insert newLeads;
    }

    @IsTest
    static void testLeadProcessor() {
        Test.startTest();
        LeadProcessor leadProcessor = new LeadProcessor();
        Database.executeBatch(leadProcessor);
        Test.stopTest();
        System.assertEquals(200, [SELECT COUNT() FROM Lead WHERE LeadSource = 'Dreamforce']);
    }
}