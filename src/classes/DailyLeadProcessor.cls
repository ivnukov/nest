/**
 * Created by ivnuk on 12.12.19.
 */

global class DailyLeadProcessor implements Schedulable {
    global void execute(SchedulableContext ctx) {
        List<Lead> leadsForUpdate = [SELECT Id, LeadSource FROM Lead WHERE LeadSource = NULL LIMIT 200];
        for (Lead lead : leadsForUpdate) {
            lead.LeadSource = 'Dreamforce';
        }
        update leadsForUpdate;
    }

}