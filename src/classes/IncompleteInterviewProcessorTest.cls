@IsTest
public with sharing class IncompleteInterviewProcessorTest {

    @TestSetup
    static void setup() {

        String orgId = UserInfo.getOrganizationId();
        String dateString =
                String.valueOf(Datetime.now()).replace(' ', '').replace(':', '').replace('-', '');
        Integer randomInt = Integer.valueOf(Math.rint(Math.random() * 1000000));
        String uniqueName = orgId + dateString + randomInt;

        User recruiter = new User(
                LastName = 'Recruiter',
                Email = uniqueName + '@test.org',
                Username = uniqueName + '@test' + orgId + '.org',
                Alias = 'TestR',
                TimeZoneSidKey = 'America/New_York',
                LocaleSidKey = 'en_US',
                EmailEncodingKey = 'UTF-8',
                LanguageLocaleKey = 'en_US',
                ProfileId = [SELECT Id FROM Profile WHERE Name = 'Standard User'].Id
        );

        insert recruiter;

        Account account = new Account(Name = 'Test Account');
        insert account;

        List<Contact> contacts = new List<Contact>();
        for (Integer i = 0; i < 5; i++) {
            contacts.add(new Contact(
                    LastName = 'Contact ' + i,
                    AccountId = account.Id
            ));
        }
        insert contacts;

        Vacancy__c vacancy = new Vacancy__c(
                Account__c = account.Id,
                Minimum_Salary__c = 1000,
                Primary_Skill__c = 'Python',
                Primary_Skill_Level__c = 'intermediate',
                Short_Description__c = 'Test',
                Assignee__c = recruiter.Id,
                Stakeholder__c = contacts[0].Id,
                Title__c = 'Test Title'
        );

        insert vacancy;

        Candidate__c candidate = new Candidate__c(
                First_Name__c = 'Test',
                Last_Name__c = 'Candidate',
                English_Level__c = 'Intermediate',
                Phone_Number__c = '12324412',
                Email__c = 'example@email.com',
                Primary_Skill__c = 'Python',
                Primary_Skill_Level__c = 'Intermediate',
                Expected_Salary__c = 1000
        );
        insert candidate;

        Interview__c interview = new Interview__c(
                Candidate__c = candidate.Id,
                Interview_Time__c = Datetime.now().addDays(-3),
                Vacancy__c = vacancy.Id,
                Status__c = 'Completed'
        );
        insert interview;

        List<Feedback__c> feedbacks = new List<Feedback__c>();

        for (Integer i = 0; i < 3; i++) {
            feedbacks.add(new Feedback__c(
                    Interview__c = interview.Id,
                    Interviewer__c = contacts[i].Id));
        }

        insert feedbacks;
    }

    @IsTest
    static void testIncompleteInterviewProcessorBatch() {

        Test.startTest();
        Database.executeBatch(new IncompleteInterviewProcessor());
        Test.stopTest();

        System.assertEquals(1, [SELECT COUNT() FROM Task]);
    }

    @IsTest
    static void testIncompleteInterviewProcessorBatchThreeCompletedFeedbacks() {
        Candidate__c candidate = new Candidate__c(
                First_Name__c = 'Test',
                Last_Name__c = 'Candidate',
                English_Level__c = 'Intermediate',
                Phone_Number__c = '12324412',
                Email__c = 'example1@email.com',
                Primary_Skill__c = 'Python',
                Primary_Skill_Level__c = 'Intermediate',
                Expected_Salary__c = 1000
        );
        insert candidate;

        Interview__c interview = new Interview__c(
                Candidate__c = candidate.Id,
                Interview_Time__c = Datetime.now().addDays(-3),
                Vacancy__c = [SELECT Id FROM Vacancy__c LIMIT 1].Id,
                Status__c = 'Completed'
        );
        insert interview;

        List<Feedback__c> feedbacks = new List<Feedback__c>();

        List<Contact> contacts = [SELECT Id FROM Contact];

        for (Integer i = 0; i < 5; i++) {
            Feedback__c fb = new Feedback__c(
                    Interview__c = interview.Id,
                    Interviewer__c = contacts[i].Id);
            if (i >= 2) {
                fb.Tech_Skills_Feedback__c = 'Some text';
                fb.Tech_Skills_Grade__c = '10';
                fb.Summary__c = 'Recommend';
            }
            feedbacks.add(fb);
        }
        insert feedbacks;

        Test.startTest();
        Database.executeBatch(new IncompleteInterviewProcessor());
        Test.stopTest();
        System.assertEquals(1, [SELECT COUNT() FROM Task]);
    }
}