@IsTest
public with sharing class AccountProcessorTest {
//Created for trailhead.
    @TestSetup
    static void setup() {
        List<Account> accounts = new List<Account>();

        for (Integer i = 0; i < 3; i++) {
            accounts.add(new Account(Name = 'Account' + i));
        }

        insert accounts;

        List<Contact> contacts = new List<Contact>();
        for (Account account : accounts) {
            for (Integer i = 0; i < 3; i++) {
                Contact newContact = new Contact(
                        FirstName = 'Test',
                        LastName = 'Contact' + i,
                        Email = 'email' + i + account.Id + '@example.com',
                        AccountId = account.Id);
                contacts.add(newContact);
            }
        }
        insert contacts;
        List<Account> accList = [SELECT Id, Number_of_Contacts__c, (SELECT Id FROM Contacts) FROM Account];
        for (Account acc : accList) {
            System.debug('Account: ' + acc.Contacts);
        }

    }

    @IsTest
    static void testContactUpdate() {
        List<Id> accIds = new List<Id>();
        List<Account> accList = [SELECT Id, Number_of_Contacts__c, (SELECT Id FROM Contacts) FROM Account];
        for (Account acc : accList) {
            accIds.add(acc.Id);
        }

        Test.startTest();
        AccountProcessor.countContacts(accIds);
        Test.stopTest();
        Account firstAccount = [SELECT Id, Number_of_Contacts__c FROM Account LIMIT 1];
        System.assertEquals(3, firstAccount.Number_of_Contacts__c);
    }
}