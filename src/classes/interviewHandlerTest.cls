@IsTest
public with sharing class interviewHandlerTest {
    @TestSetup
    static void setup() {
        String orgId = UserInfo.getOrganizationId();
        String dateString =
                String.valueOf(Datetime.now()).replace(' ', '').replace(':', '').replace('-', '');
        Integer randomInt = Integer.valueOf(Math.rint(Math.random() * 1000000));
        String uniqueName = orgId + dateString + randomInt;

        User recruiter = new User(
                LastName = 'Recruiter',
                Email = uniqueName + '@test.org',
                Username = uniqueName + '@test' + orgId + '.org',
                Alias = 'TestR',
                TimeZoneSidKey = 'America/New_York',
                LocaleSidKey = 'en_US',
                EmailEncodingKey = 'UTF-8',
                LanguageLocaleKey = 'en_US',
                ProfileId = [SELECT Id FROM Profile WHERE Name = 'Standard User'].Id
        );

        insert recruiter;

        Account account = new Account(Name = 'Test Account');
        insert account;

        List<Contact> contacts = new List<Contact>();
        for (Integer i = 0; i < 5; i++) {
            contacts.add(new Contact(
                    LastName = 'Contact ' + i,
                    AccountId = account.Id
            ));
        }
        insert contacts;

        List<Vacancy__c> vacancies = new List<Vacancy__c>();

        for (Integer i = 0; i < 200; i++) {
            vacancies.add(new Vacancy__c(
                    Account__c = account.Id,
                    Minimum_Salary__c = 1000,
                    Primary_Skill__c = 'Python',
                    Primary_Skill_Level__c = 'intermediate',
                    Short_Description__c = 'Test',
                    Assignee__c = recruiter.Id,
                    Stakeholder__c = contacts[0].Id,
                    Title__c = 'Test Title'
            ));
        }

        insert vacancies;

        List<Candidate__c> candidates = new List<Candidate__c>();
        for (Integer i = 0; i < 200; i++) {
            candidates.add(new Candidate__c(
                    First_Name__c = 'Test',
                    Last_Name__c = 'Candidate' + i,
                    English_Level__c = 'Intermediate',
                    Phone_Number__c = '12324412',
                    Email__c = 'example' + i + '@email.com',
                    Primary_Skill__c = 'Python',
                    Primary_Skill_Level__c = 'Intermediate',
                    Expected_Salary__c = 1000
            ));
        }
        insert candidates;

        List<Interview__c> interviews = new List<Interview__c>();
        List<Feedback__c> feedbacks = new List<Feedback__c>();

        for (Integer i = 0; i < 200; i++) {
            interviews.add(new Interview__c(
                    Vacancy__c = vacancies[i].Id,
                    Candidate__c = candidates[i].Id,
                    Status__c = 'Completed',
                    Interview_Time__c = Datetime.now().addDays(-1)
            ));
        }

        insert interviews;

        for (Integer i = 0; i < 200; i++) {
            for (Integer j = 0; j < 3; j++) {
                feedbacks.add(new Feedback__c(
                        Interview__c = interviews[i].Id,
                        Interviewer__c = contacts[Math.mod(i, 5)].Id,
                        Tech_Skills_Feedback__c = 'Some text',
                        Tech_Skills_Grade__c = '10',
                        Summary__c = 'Recommend'));

            }
        }
        insert feedbacks;
    }

    @IsTest
    static void testInterviewHandler() {
// TODO update setup to hard test
        Test.startTest();
        List<Interview__c> interviews = [SELECT Id, Result__c FROM Interview__c];
        for (Interview__c interview : interviews) {
            interview.Result__c = 'Hired';
        }
        update interviews;
        Test.stopTest();

        System.assertEquals(200, [SELECT COUNT() FROM Interview__c WHERE Result__c = 'Hired']);
        System.assertEquals(200, [SELECT COUNT() FROM Vacancy__c WHERE Closed__c = TRUE]);
    }

    @IsTest
    static void testInterviewHandlerNegativeCase() {

        Interview__c interview = new Interview__c(
                Vacancy__c = [SELECT Id FROM Vacancy__c LIMIT 1].Id,
                Candidate__c = [SELECT Id FROM Candidate__c LIMIT 1].Id,
                Status__c = 'Completed',
                Interview_Time__c = Datetime.now().addDays(-1));
        insert interview;

        List<Contact> contacts = [SELECT Id FROM Contact LIMIT 3];

        List<Feedback__c> feedbacks = new List<Feedback__c>();

        for (Integer i = 0; i < 3; i++) {
            feedbacks.add(new Feedback__c(
                    Interview__c = interview.Id,
                    Interviewer__c = contacts[i].Id
            ));
        }

        Test.startTest();
        interview.Result__c = 'Hired';
        DmlException error;
        try {
            update interview;

        } catch (DmlException err) {
            error = err;
        }
        Test.stopTest();

        System.assertEquals('FIELD_CUSTOM_VALIDATION_EXCEPTION',
                error.getDmlStatusCode(0));
        System.assert(error.getMessage().contains('Interview should have at least three feedback entries to be closed'));
    }

    @IsTest
    static void interviewHandlerChangeClosedInterviewTest() {
        DmlException error;
        Interview__c interview = [SELECT Id, Result__c FROM Interview__c LIMIT 1];
        interview.Result__c = 'Hired';

        update interview;

        Test.startTest();
        interview.Result__c = 'Declined';
        try {
            update interview;

        } catch (DmlException err) {
            error = err;
        }

        Test.stopTest();
        System.assertEquals('FIELD_CUSTOM_VALIDATION_EXCEPTION',
                error.getDmlStatusCode(0));
        System.assert(error.getMessage().contains('You can\'t change interview state.'));
    }
}