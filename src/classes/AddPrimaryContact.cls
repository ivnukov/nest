public class AddPrimaryContact implements Queueable {
//Created for trailhead.
    Contact contact;
    String stateAbbr;

    public AddPrimaryContact(Contact contactObj, String state) {
        contact = contactObj;
        stateAbbr = state;
    }

    public void execute(QueueableContext context) {
        List<Account> accountsList = [SELECT Id, BillingState FROM Account WHERE BillingState = :stateAbbr LIMIT 200];
        List<Contact> contactsList = new List<Contact>();
        for (Account account : accountsList) {
            Contact newContact = contact.clone();
            newContact.AccountId = account.Id;
            contactsList.add(newContact);
        }

        insert contactsList;
    }
}