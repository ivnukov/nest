/**
 * Created by ivnuk on 09.12.19.
 */

trigger InterviewTrigger on Interview__c (before update) {
    if (Trigger.isBefore) {
        if (Trigger.isUpdate) {
            InterviewHandler handler = new InterviewHandler();
            handler.validateFeedback();
            handler.updateVacancy();
        }
    }
}