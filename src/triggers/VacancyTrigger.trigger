trigger VacancyTrigger on Vacancy__c (before insert, before update) {
    /**
    * Set minimal salary as maximum if maximum is not set.
    */
    if (Trigger.isBefore) {
        // All "before" triggers should be handled here.
        if (Trigger.isUpdate || Trigger.isInsert) {
            new MaximumSalaryHandler().setMaxSalary();
        }
    }
}